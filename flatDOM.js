//@ts-check
const { clone, filter, reject } = require("ramda")
const { dig } = require("svpr")

function flatDOM(query) {
  let flatten = []
  const $ = query.$

	function addItem(query) {
		let children = query.get(0).children
    let textChildren = filter(c => c.type === "text", children)
    let textData = ""
    if (textChildren.length > 0) textData = textChildren.map(textChild => textChild.data).join("\n")
    flatten.push({
      selector: query.selector().value(),
      uniqueSelector: query.uniqueSelector().value(),
      fullSelector: query.fullSelector().value(),
      text: textData,
      context: query.get(0),
      query: $(query)
    })
  }

  addItem(query)

  dig(node => {
    let localQuery = $(node)
    addItem(localQuery)
    let childrenExceptTexts = []
    if (localQuery.get(0).children.length > 0) {
      childrenExceptTexts = reject(c => c.type === "text", localQuery.get(0).children)
    }
    return childrenExceptTexts
  }, query.children().get())

  query.set(flatten)
  return query
}

module.exports = { flatDOM }
